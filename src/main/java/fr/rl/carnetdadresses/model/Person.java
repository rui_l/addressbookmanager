package fr.rl.carnetdadresses.model;

import javafx.beans.property.*;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;

import static fr.rl.carnetdadresses.util.DateUtil.parse;

public class Person {
    private final StringProperty firstName;
    private final StringProperty lastName;
    private final StringProperty street;
    private final IntegerProperty postalCode;
    private final StringProperty city;
    private final ObjectProperty<LocalDate> birthday;

    /**
     * Default constructor.
     */
    public Person() {
        this(null, null);
    }

    /**
     * Constructor with some initial data.
     * @param firstName
     * @param lastName
     */
    public Person(String firstName, String lastName) {
        if (firstName != null) {
            this.firstName = new SimpleStringProperty(StringUtils.capitalize(firstName));
        } else {
            this.firstName = new SimpleStringProperty(null);
        }
        if (lastName != null) {
            this.lastName = new SimpleStringProperty(lastName.toUpperCase());
        } else {
            this.lastName = new SimpleStringProperty(null);
        }
        // Some initial dummy data, just for convenient testing.
        this.street = new SimpleStringProperty("some street");
        this.postalCode = new SimpleIntegerProperty(12345);
        this.city = new SimpleStringProperty("some city");
        this.birthday = new SimpleObjectProperty<>(LocalDate.of(2023, 10, 11));
    }

    public Person(String nom, String prenom, String dateNaissance) {
        this.firstName = new SimpleStringProperty(StringUtils.capitalize(prenom));
        this.lastName = new SimpleStringProperty(nom.toUpperCase());
        this.birthday = new SimpleObjectProperty<>(parse(dateNaissance));
        this.street = new SimpleStringProperty("some street");
        this.postalCode = new SimpleIntegerProperty(12345);
        this.city = new SimpleStringProperty("some city");
    }

//    public Person(String lastName, String firstName, LocalDate birthday) {}

    public String getFirstName() {
        return firstName.get();
    }

    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    public StringProperty firstNameProperty() {
        return firstName;
    }

    public String getLastName() {
        return lastName.get();
    }

    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }

    public StringProperty lastNameProperty() {
        return lastName;
    }

    public String getStreet() {
        return street.get();
    }

    public void setStreet(String street) {
        this.street.set(street);
    }

    public StringProperty streetProperty() {
        return street;
    }

    public int getPostalCode() {
        return postalCode.get();
    }

    public void setPostalCode(int postalCode) {
        this.postalCode.set(postalCode);
    }

    public IntegerProperty postalCodeProperty() {
        return postalCode;
    }

    public String getCity() {
        return city.get();
    }

    public void setCity(String city) {
        this.city.set(city);
    }

    public StringProperty cityProperty() {
        return city;
    }


    public LocalDate getBirthday() {
        return birthday.get();
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday.set(birthday);
    }

    public ObjectProperty<LocalDate> birthdayProperty() {
        return birthday;
    }

}
