package fr.rl.carnetdadresses.controller;

import fr.rl.carnetdadresses.ContactFX;
import fr.rl.carnetdadresses.dao.ParametreDAO;
import fr.rl.carnetdadresses.dao.RepertoireBean;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.stage.FileChooser;

import java.io.File;
import java.util.Optional;

/**
 * The controller for the MenuLayout.
 * The MenuLayout provides the basic application layout containing a menu bar and space
 * where other JavaFX elements can be placed.
 */
public class MenuController {
    // Reference to the main application
    @FXML 
    private ContactFX contactFX;
    @FXML
    private Menu menuFichiersRecents;
    @FXML
    private MenuItem menuItemSave;
    @FXML
    private MenuItem menuItemSaveAs;
    private ParametreDAO parametreDAO;

    private void initialize() {
         disableItems(true);
    }

    private void disableItems(boolean b) {
        /*menuItemSave.setDisable(b);
        menuItemSaveAs.setDisable(b);*/
    }

    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param contactFX the main application.
     */
    public void setContactFX(ContactFX contactFX) {
        this.contactFX = contactFX;
        initialize();
        contactFX.getPrimaryStage().setOnCloseRequest(event -> sauvegarder());
        genererMenuFichiersRecents();
    }

    private void genererMenuFichiersRecents() {
    }

    private void sauvegarder() {
        RepertoireBean repertoireBean = contactFX.getRepertoireBean();
        if (repertoireBean!=null && repertoireBean.isSaved()) {
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Modifications détectées ! ");
            alert.setContentText(String.format("Le fichier %s a été modifié, \nvoulez-vous enregistrer les modifications ?", repertoireBean.getFile().getName()));
            Optional<ButtonType> response = alert.showAndWait();
            if (response.isPresent() && response.get()==ButtonType.OK) {
                repertoireBean.sauver();
            }
        }
    }

    /**
     * Creates an empty address book.
     */
    @FXML
    private void handleNew() {
        //contactFX.setRepertoireBean();
        //contactFX.setPersonFilePath(null);
    }

    /**
     * Opens a FileChooser to let the user select an address book to load.
     */
    @FXML
    private void handleOpen() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Ouvrir un fichier de contacts ");
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);
        File selectedFile = fileChooser.showOpenDialog(contactFX.getPrimaryStage());
        if (selectedFile != null) {
            contactFX.setRepertoireBean(selectedFile);
        }
    }

    /**
     * Saves the file to the person file that is currently open. If there is no
     * open file, the "save as" dialog is shown.
     */
    @FXML
    private void handleSave() {
        /*File personFile = contactFX.getPersonFilePath();
        if (personFile != null) {
            contactFX.savePersonDataToFile(personFile);
        } else {
            handleSaveAs();
        }*/
    }

    /**
     * Opens a FileChooser to let the user select a file to save to.
     */
    @FXML
    private void handleSaveAs() {
        /*FileChooser fileChooser = new FileChooser();

        // Set extension filter
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");
        fileChooser.getExtensionFilters().add(extFilter);

        // Show save file dialog
        File file = fileChooser.showSaveDialog(contactFX.getPrimaryStage());

        if (file != null) {
            // Make sure it has the correct extension
            if (!file.getPath().endsWith(".xml")) {
                file = new File(file.getPath() + ".xml");
            }
            contactFX.savePersonDataToFile(file);
        }*/
    }

    /**
     * Opens an About dialog.
     */
    @FXML
    private void handleAbout() {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Addresses");
        alert.setHeaderText("About");
        alert.setContentText("Author: L\nWebsite: https://apprenti-javaFX");

        alert.showAndWait();
    }

    /**
     * Closes the application.
     */
    @FXML
    private void handleClose() {
        sauvegarder();
        Platform.exit();
    }
}
