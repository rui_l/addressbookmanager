package fr.rl.carnetdadresses.controller;

import fr.rl.carnetdadresses.model.Person;
import fr.rl.carnetdadresses.util.DateUtil;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.apache.commons.lang3.StringUtils;

/**
 * Dialog to edit details of a person.
 *
 * @author Marco Jakob
 */
public class PersonEditDialogController {
    @FXML
    private TextField firstNameField;
    @FXML
    private TextField lastNameField;
    @FXML
    private TextField streetField;
    @FXML
    private TextField postalCodeField;
    @FXML
    private TextField cityField;
    @FXML
    private TextField birthdayField;


    private Stage dialogStage;
    private Person person;
    private boolean okClicked = false;

    /**
     * Initializes the controller class. This method is automatically called after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {
        // la fonction initialize(), qui ne doit pas accepter d’arguments et retourner le type void.
        // Une fois le processus de chargement du document FXML terminé, le chargeur FXML appelle la fonction initialize().
    }

    /**
     * Sets the stage of this dialog.
     *
     * @param dialogStage The stage to be displayed to the user.
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * Sets the person to be edited in the dialog.
     *
     * @param person This person's information will be shown in the stage.
     */
    public void setPerson(Person person) {
        this.person = person;

        firstNameField.setText(StringUtils.capitalize(person.getFirstName()));
        lastNameField.setText(person.getLastName().toUpperCase());
        streetField.setText(person.getStreet());
        postalCodeField.setText(Integer.toString(person.getPostalCode()));
        cityField.setText(person.getCity());
        birthdayField.setText(DateUtil.format(person.getBirthday()));
        birthdayField.setPromptText("dd/mm/yyyy"); //question: pourquoi jamais utilisée ?
    }

    /**
     * Returns true if the user clicked OK, false otherwise.
     *
     * @return true if the user clicked OK, false if the user did not click OK.
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * Called when the user clicks ok.
     */
    @FXML
    private void handleOk() {
        if (isInputValid()) {
            person.setFirstName(firstNameField.getText());
            person.setLastName(lastNameField.getText());
            person.setStreet(streetField.getText());
            person.setPostalCode(Integer.parseInt(postalCodeField.getText()));
            person.setCity(cityField.getText());
            person.setBirthday(DateUtil.parse(birthdayField.getText()));

            okClicked = true;
            dialogStage.close();
        }
    }

    /**
     * Called when the user clicks cancel.
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    /**
     * Validates the user input in the text fields.
     *
     * @return true if the input is valid
     */
    private boolean isInputValid() {
        String errorMessage = "";

        errorMessage = setErrorMessage(errorMessage);

        if (errorMessage.isEmpty()) {
            return true;
        } else {
            // Show the error message.
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Invalid Fields");
            alert.setHeaderText("Please correct invalid fields");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }

    private String setErrorMessage(String errorMessage) {
        if (isNotAValidText(firstNameField)) {
            errorMessage += "No valid first name!\n";
        }
        if (isNotAValidText(lastNameField)) {
            errorMessage += "No valid last name!\n";
        }
        if (isNotAValidText(streetField)) {
            errorMessage += "No valid street!\n";
        }

        if (isNotAValidText(postalCodeField)) {
            errorMessage += "No valid postal code!\n";
        } else {
            // try to parse the postal code into an int.
            try {
                Integer.parseInt(postalCodeField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "No valid postal code (must be an integer)!\n";
            }
        }

        if (isNotAValidText(cityField)) {
            errorMessage += "No valid city!\n";
        }

        if (isNotAValidText(birthdayField)) {
            errorMessage += "No valid birthday!\n";
        } else {
            if (!DateUtil.validDate(birthdayField.getText())) {
                errorMessage += "No valid birthday. Use the format dd/mm/yyyy!\n";
            }
        }
        return errorMessage;
    }

    private boolean isNotAValidText(TextField textField) {
        return textField.getText() == null || textField.getText().isEmpty();
    }
}
