package fr.rl.carnetdadresses.dao;

import fr.rl.carnetdadresses.model.Person;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;

import java.io.File;
import java.util.ArrayList;

public class RepertoireBean {
    private ObservableList<Person> listeOfAllContacts;
    private FilteredList<Person> listeFiltree;
    private SortedList<Person> listeTriee;
    private File file;
    private ContactDAO contactDAO;
    private Person selectedPerson;
    private Boolean saved;
    private Person searchedPerson;

    public RepertoireBean(File file) {
        searchedPerson = new Person("","","01/01/2030");
        this.file = file;
        contactDAO = new ContactDAO(file);
        listeOfAllContacts = FXCollections.observableArrayList(contactDAO.getAllContacts());
        listeFiltree = new FilteredList<>(listeOfAllContacts,null);
        listeTriee = new SortedList<>(listeFiltree);
        saved = true;
    }

    public void filterContact() {
        listeFiltree.setPredicate(person -> {
            boolean c1 = person.getLastName().toUpperCase().contains(searchedPerson.getLastName().toUpperCase());
            boolean c2 = person.getFirstName().toLowerCase().contains(searchedPerson.getFirstName().toLowerCase());
            return c1 && c2;
        });
    }

    public boolean isSaved() {
        return this.saved;
    }

    public File getFile() {
        return this.file;
    }

    public ObservableList<Person> getListeOfAllContacts() {
        return listeOfAllContacts;
    }

    public void sauver() {
        contactDAO.writeAllContactsOnText(listeOfAllContacts);
        saved = true;
    }
}
