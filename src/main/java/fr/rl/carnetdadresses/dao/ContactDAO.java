package fr.rl.carnetdadresses.dao;

import fr.rl.carnetdadresses.model.Person;
import javafx.collections.ObservableList;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ContactDAO {
    private FichierTexte fichierTexte;

    public ContactDAO(File file) {
        fichierTexte = new FichierTexte(file);
    }

    public List<Person> getAllContacts() {
        List<Person> contacts = new ArrayList<>();
        List<String> lignes = fichierTexte.lire();
        for (String ligne : lignes) {
            contacts.add(stringToPerson(ligne));
        }
        return contacts;
    }

    private Person stringToPerson(String ligne) {
        List<String> parse;
        parse = Arrays.asList(ligne.split("\\|"));
        String nom = parse.get(0);
        String prenom = (parse.size()>1) ? parse.get(1):"";
        String dateNaissance = (parse.size()>2) ? parse.get(2) : "01/01/1900";
        return new Person(nom, prenom, dateNaissance);
    }

    public void writeAllContactsOnText(ObservableList<Person> contacts) {
        List<String> lignes = new ArrayList<>();
        for (Person personne:contacts) {
            lignes.add(personToString(personne));
        }
        fichierTexte.ecrire(lignes);
    }

    /**
     * Transforme d'une Person en chaîne de caractères sous format "NOM|Prenom|dd/mm/yyyy".
     *
     * @param person de classe Person.
     * @return une chaîne de caractères.
     */
    private String personToString(Person person) {
        return person.getLastName()+"|"+person.getFirstName()+"|"+person.getBirthday();
    }

}
