package fr.rl.carnetdadresses.dao;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * traite l'accès avec le système d'exploitation.
 */
public class FichierTexte {
    private File file;

    public FichierTexte(File file) {
        this.file = file;
        if (!file.exists()) {
            try {
                if (file.createNewFile())
                    System.out.println("Fichier créé ! ");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public List<String> lire() {
        List<String> lignes = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))){
            String ligne;
            while ((ligne = bufferedReader.readLine()) != null) {
                lignes.add(ligne);
            }
        } catch (IOException e) {
            // nothing to do
        }
        return lignes;
    }

    public void ecrire(List<String> lignes) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file))) {
            for (String ligne : lignes) {
                bufferedWriter.append(ligne);
                bufferedWriter.newLine();
            }
        } catch (IOException e) {
            // nothing to do
        }
    }


}
