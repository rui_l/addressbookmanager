package fr.rl.carnetdadresses;

import fr.rl.carnetdadresses.controller.MenuController;
import fr.rl.carnetdadresses.controller.PersonOverviewController;
import fr.rl.carnetdadresses.dao.RepertoireBean;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;

public class ContactFX extends Application {
    private Stage primaryStage;
    private BorderPane menuLayout;
    private RepertoireBean repertoireBean;

    // Point d'entrée de l'application :
    public static void main (String[] args) {
        launch(args);
    }

    // Getters:
    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public BorderPane getMenuLayout() {
        return menuLayout;
    }

    public RepertoireBean getRepertoireBean() {
        return repertoireBean;
    }

    // setter:
    public void setRepertoireBean(File file) {
        repertoireBean = new RepertoireBean(file);
        showPersonOverview();
    }

    private void showPersonOverview() {
        try {
            // Load person overview.
            FXMLLoader myLoader = new FXMLLoader();
            myLoader.setLocation(ContactFX.class.getResource("PersonOverview.fxml"));
            AnchorPane personOverview = myLoader.load();
            // Set person overview into the center of Menulayout.
            menuLayout.setCenter(personOverview);
            // Give the controller access to the principal application ContactFX.
            PersonOverviewController controller = myLoader.getController();
            controller.setContactFX(this);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }


    @Override
    public void start(Stage stage) throws Exception {
        this.primaryStage = stage;
        this.primaryStage.setTitle("Carnet d'adresses");
        this.primaryStage.getIcons().add(new Image("file:src/main/resources/images/carnet_adresses_icon.png"));
        showMenu();
    }

    private void showMenu() throws RuntimeException {
        try {
            FXMLLoader myLoader = new FXMLLoader();
            myLoader.setLocation(ContactFX.class.getResource("MenuLayout.fxml"));
            this.menuLayout = myLoader.load();

            Scene scene = new Scene(this.menuLayout);
            this.primaryStage.setScene(scene);

            MenuController controller = myLoader.getController();
            controller.setContactFX(this);

            this.primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }


}
