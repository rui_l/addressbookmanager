module fr.rl.carnetdadresses {
    requires javafx.controls;
    requires javafx.fxml;
    requires org.apache.commons.lang3;
    requires java.scripting;


    opens fr.rl.carnetdadresses to javafx.fxml;
    exports fr.rl.carnetdadresses;
    opens fr.rl.carnetdadresses.controller to javafx.fxml;
    exports fr.rl.carnetdadresses.controller;
}